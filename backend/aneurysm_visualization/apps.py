from django.apps import AppConfig


class AneurysmVisualizationConfig(AppConfig):
    name = 'aneurysm_visualization'
