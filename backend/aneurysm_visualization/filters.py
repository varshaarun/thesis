from .models import PatientData
from django_filters import rest_framework as filters


class PatientFilter(filters.FilterSet):
    class Meta:
        model = PatientData
        fields = '__all__'
        #fields = ['sex', 'aneurysmType', 'ruptureStatus']