from .models import PatientData
from .queries import query_generator


def get_patient_details(query):
    patient_list = []
    # query = 'Select * from aneurysm_visualization_patientdata where aneurysmLocation= "M1" and sex = "F" and (age between 24 and 70) and (rupturestatus = "R" or rupturestatus = "U")'
    patient_data = PatientData.objects.raw(query)
    for patient in patient_data:
        patient_dict = {'id': patient.id, 'institution': patient.institution, 'modality': patient.modality,
                        'age': patient.age, 'sex': patient.sex, 'aneurysmType': patient.aneurysmType,
                        'aneurysmLocation': patient.aneurysmLocation, 'ruptureStatus': patient.ruptureStatus,
                        'multipleAneurysms': patient.multipleAneurysms, 'medicalHistory': patient.medicalHistory,
                        'Comment': patient.Comment, 'avId': patient.avId}
        patient_list.append(patient_dict)
    return patient_list


def query_generator(data, operator):
    outer_string = ''

    for i in range(0, len(data)):
        key = data[i]['key']
        value = data[i]['value']
        inner_string = ''
        if key != 'age':
            inner_string += '('
            for val in value:
                if inner_string != '(': inner_string += ' or '
                inner_string += key + '="' + val + '"'
            inner_string += ')'
        if key == 'age':
            inner_string = '(' + key + ' between ' + str(value[0]) + ' and ' + str(value[1]) + ')'
        if outer_string != '':  # outer_string += ' and '
            outer_string += operator.lower()
        outer_string += inner_string
    outer_string = '(' + outer_string + ')'
    query = 'SELECT * from aneurysm_visualization_patientdata where ' + outer_string
    patient_list = get_patient_details(query)
    return patient_list


def request_parser(data):
    """
    {"key":"Sex","value":["F"]},
	{"key":"age","value":[25,70]}
	"""
    outer_string = ''
    patient_details = {}
    if len(data) != 0:
        key = data['key']
        value = data['value']
        if 'dependsOn' in data.keys():
            for value in data['value']:
                for depends_on_data in data['dependsOn']:
                    inner_string = ''
                    if depends_on_data['key'] != 'age':
                        inner_string += '('
                        for val in depends_on_data['value']:
                            if inner_string != '(': inner_string += ' or '
                            inner_string += depends_on_data['key'] + '="' + val + '"'
                        inner_string += ')'
                    if key == 'age':
                        inner_string = '(' + key + ' between ' + str(value[0]) + ' and ' + str(value[1]) + ')'
                    if outer_string != '':  outer_string += ' and '
                    outer_string += inner_string
                outer_string = '(' + outer_string + ')'
                if data["location"] == "ophthal":
                    query = 'SELECT * from aneurysm_visualization_patientdata where  (aneurysmLocation = "infraophth" or aneurysmLocation = "paraophth")' + ' and ' + key + ' = "' + value + '" and ' + outer_string
                else:
                    query = 'SELECT * from aneurysm_visualization_patientdata where  aneurysmLocation = "' + data[
                        "location"] + '" and ' + key + ' = "' + value + '" and ' + outer_string
                patient_list = get_patient_details(query)
                patient_details.update({value: patient_list})

        else:
            if key == 'age':
                if data["location"] == "ophthal":
                    query = 'SELECT * from aneurysm_visualization_patientdata where  (aneurysmLocation = "infraophth" or aneurysmLocation = "paraophth")' + ' and ' + key + ' between ' + str(data['value'][0]) + ' and ' + str(data['value'][1])
                else:
                    query = 'SELECT * from aneurysm_visualization_patientdata where aneurysmLocation = "' + data[
                        "location"] + '" and ' + key + ' between ' + str(data['value'][0]) + ' and ' + str(data['value'][1])
                patient_list = get_patient_details(query)
                patient_details.update({key: patient_list})
            else:
                for value in data['value']:
                    if data["location"] == "ophthal":
                        query = 'SELECT * from aneurysm_visualization_patientdata where  (aneurysmLocation = "infraophth" or aneurysmLocation = "paraophth")' + ' and ' + key + ' = "' + value + '"'
                    else:
                        query = 'SELECT * from aneurysm_visualization_patientdata where aneurysmLocation = "' + data[
                            "location"] + '" and ' + key + ' = "' + value + '"'
                    patient_list = get_patient_details(query)
                    patient_details.update({value: patient_list})
    else:

        patient_details.update({data["location"]: {}})
    return patient_details
