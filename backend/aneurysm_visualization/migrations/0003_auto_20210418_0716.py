# Generated by Django 3.1.7 on 2021-04-18 05:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('aneurysm_visualization', '0002_auto_20210418_0706'),
    ]

    operations = [

        migrations.AlterField(
            model_name='patientdata',
            name='AvId',
            field=models.AutoField(default=0, primary_key=True, serialize=False),
        ),
    ]
