from django.db import models


# Create your models here.
class PatientData(models.Model):
    avId = models.CharField(max_length=100, default='null', primary_key=True)
    id = models.CharField(max_length=100, default='null')
    institution = models.CharField(max_length=100, default='null')
    modality = models.CharField(max_length=100, default='null')
    age = models.CharField(max_length=100, default='null')
    sex = models.CharField(max_length=20, default='null')
    aneurysmType = models.CharField(max_length=20, default='null')
    aneurysmLocation = models.CharField(max_length=20, default='null')
    ruptureStatus = models.CharField(max_length=20, default='null')
    multipleAneurysms = models.CharField(max_length=20, default='null')
    medicalHistory = models.CharField(max_length=100, default='null')
    Comment = models.CharField(max_length=100, default='null')
