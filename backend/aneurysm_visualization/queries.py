from typing import Dict, Any

from .models import PatientData


def distinct_values():
    distinct_values_dict = {}
    if PatientData.objects.all().count() == 0:
        distinct_values_dict.update({'msg': 'data base empty. Please upload a CSV file'})
    else:
        columns = PatientData.objects.raw('SELECT * FROM aneurysm_visualization_patientdata').columns
        for i in columns:
            if i != 'id' and i != 'age' and i != 'avId':
                distinct_values_dict[i] = PatientData.objects.values_list(i, flat=True).distinct()
    return distinct_values_dict


# get data from database based on the query
def get_patient_details(query):
    patient_list = []
    # query = 'Select * from aneurysm_visualization_patientdata where aneurysmLocation= "M1" and sex = "F" and (age between 24 and 70) and (rupturestatus = "R" or rupturestatus = "U")'
    patient_data = PatientData.objects.raw(query)
    for patient in patient_data:
        patient_dict = {'id': patient.id, 'institution': patient.institution, 'modality': patient.modality,
                        'age': patient.age, 'sex': patient.sex, 'aneurysmType': patient.aneurysmType,
                        'aneurysmLocation': patient.aneurysmLocation, 'ruptureStatus': patient.ruptureStatus,
                        'multipleAneurysms': patient.multipleAneurysms, 'medicalHistory': patient.medicalHistory,
                        'Comment': patient.Comment, 'avId': patient.avId}
        patient_list.append(patient_dict)
    return patient_list


def query_generator(data, loc, operator):
    patient_details = {}
    outer_string = ''
    if len(data) != 0:
        for i in range(0, len(data)):
            key = data[i]['key']
            value = data[i]['value']
            inner_string = ''
            if key != 'age':
                inner_string += '('
                for val in value:
                    if inner_string != '(': inner_string += ' or '
                    inner_string += key + '="' + val + '"'
                inner_string += ')'
            if key == 'age':
                inner_string = '(' + key + ' between ' + str(value[0]) + ' and ' + str(value[1]) + ')'
            if outer_string != '':  # outer_string += ' and '
                outer_string += operator.lower()
            outer_string += inner_string
        outer_string = '(' + outer_string + ')'
        if loc == 'ophthal':
            location_string = '(aneurysmLocation = "infraophth" or aneurysmLocation = "paraophth")'
        else:
            location_string = 'aneurysmLocation' + '="' + loc + '"'

        query = 'SELECT * from aneurysm_visualization_patientdata where ' + location_string + ' and ' + outer_string
        patient_list = get_patient_details(query)
        patient_details.update({loc: patient_list})
    else:
        patient_details.update({loc: {}})
    return patient_details


def query_parser(data):
    response = {
        "leftFilter": {},
        "rightFilter": {},
        "count": {
            "localMaxima": {
                "leftFilter": {
                    "glyph": {"min": "", "max": ""},
                    "heatmap": {"min": "", "max": ""}
                },
                "rightFilter": {
                    "glyph": {"min": "", "max": ""},
                    "heatmap": {"min": "", "max": ""}
                },
            },
            "globalMaxima": {
                "glyph": {"min": "", "max": ""},
                "heatmap": {"min": "", "max": ""}
            },
            "overall": {
                "glyph": {"min": 0, "max": ""},
                "heatmap": {"min": 0, "max": ""}
            },
        }
    }

    min_max = {"glyph": {"min": "", "max": ""}, "heatmap": {"min": "", "max": ""}}

    count = {
        "leftFilter": {
            "glyph": [],
            "heatmap": []
        },
        "rightFilter": {
            "glyph": [],
            "heatmap": []
        }
    }

    if PatientData.objects.all().count() == 0:
        response.update({'msg': 'data base empty. Please upload a CSV file'})
    else:
        total_patients = PatientData.objects.all().count()
        for filter_side in data.keys():
            if filter_side != 'aneurysmLocation':
                for loc in data['aneurysmLocation']:
                    for key in data[filter_side].keys():
                        if 'Operator' not in key:
                            if key not in response[filter_side].keys(): response[filter_side][key] = {}
                            operator = data[filter_side][key + 'Operator']
                            patient_details = query_generator(data[filter_side][key], loc, operator)
                            response[filter_side][key].update(patient_details)
                            # response[filter_side].update(patient_details)
                            if len(patient_details) != 0:  # and (loc in count[key].keys()):
                                # for local minima
                                count[filter_side][key].append(len(patient_details[loc]))
                            else:
                                count[filter_side][key].append(0)

    # local maxima
    response["count"]["localMaxima"]["leftFilter"]["glyph"]["min"] = min(count["leftFilter"]["glyph"])
    response["count"]["localMaxima"]["leftFilter"]["glyph"]["max"] = max(count["leftFilter"]["glyph"])
    response["count"]["localMaxima"]["leftFilter"]["heatmap"]["min"] = min(count["leftFilter"]["heatmap"])
    response["count"]["localMaxima"]["leftFilter"]["heatmap"]["max"] = max(count["leftFilter"]["heatmap"])
    response["count"]["localMaxima"]["rightFilter"]["glyph"]["min"] = min(count["rightFilter"]["glyph"])
    response["count"]["localMaxima"]["rightFilter"]["glyph"]["max"] = max(count["rightFilter"]["glyph"])
    response["count"]["localMaxima"]["rightFilter"]["heatmap"]["min"] = min(count["rightFilter"]["heatmap"])
    response["count"]["localMaxima"]["rightFilter"]["heatmap"]["max"] = max(count["rightFilter"]["heatmap"])

    # global maxima
    response["count"]["globalMaxima"]["glyph"]["min"] = min(min(count["leftFilter"]["glyph"]),
                                                            min(count["rightFilter"]["glyph"]))
    response["count"]["globalMaxima"]["glyph"]["max"] = max(max(count["leftFilter"]["glyph"]),
                                                            max(count["rightFilter"]["glyph"]))
    response["count"]["globalMaxima"]["heatmap"]["min"] = min(min(count["leftFilter"]["heatmap"]),
                                                              min(count["rightFilter"]["heatmap"]))
    response["count"]["globalMaxima"]["heatmap"]["max"] = max(max(count["leftFilter"]["heatmap"]),
                                                              max(count["rightFilter"]["heatmap"]))

    # overall
    response["count"]["overall"]["glyph"]["max"] = total_patients
    response["count"]["overall"]["heatmap"]["max"] = total_patients

    return response
