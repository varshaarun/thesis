import pandas as pd


def read_csv(file):
    avid = []
    df = pd.read_csv(file)
    df = df.replace(True, 'true').replace(False, 'false').fillna('NA')
    #df['age'] = pd.to_numeric(df['age'].replace('NA', '-1'))
    df['age'] = df['age'].replace('NA', '-1').astype(int)
    count = 0
    for i in df['id']:
        avid.append(i + '-' + str(count))
        count += 1
    df['avId'] = avid

    patient_dict = df.to_dict('r')
    return patient_dict
