from django.test import TestCase, Client
from django.urls import reverse
from aneurysm_visualization.models import PatientData
from rest_framework import status
import json

# Create your tests here.
client = Client()
class PatientDataTest(TestCase):

    def setUp(self):
        self.valid_payload = {"id": "test-patient-1",
                            "institution": "UniversityHospitalMagdeburg",
                            "modality": "NA", "age": 41.0,
                            "sex": "F",
                            "aneurysmType": "TER",
                            "aneurysmLocation": "MCA-Bif",
                            "ruptureStatus": "U",
                            "multipleAneurysms": True,
                            "medicalHistory": "NA",
                            "Comment": "NA"}

        self.invalid_payload = {"id": "test-patient-2",
                            "institution": "UniversityHospitalMagdeburg",
                            "modality": "NA", "age": 41.0,
                            #"sex": "F",
                            "aneurysmType": "TER",
                            "aneurysmLocation": "MCA-Bif",
                            "ruptureStatus": "U",
                            "multipleAneurysms": True,
                            "medicalHistory": "NA",
                            "Comment": "NA"}

    def test_create_valid_puppy(self):
        response = client.post(
            reverse('add_patients_to_database'),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_puppy(self):
        response = client.post(
            reverse('add_patients_to_database'),
            data=json.dumps(self.invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)