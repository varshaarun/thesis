from django.urls import path, include
from aneurysm_visualization.views import list_all_patients, add_patients_to_database, delete_from_table, \
    get_distinct_values, query_patient_data, glyph_heatmap_request

app_name = 'patient'
urlpatterns = [
    path('list/', list_all_patients, name='list'),
    path('file/', add_patients_to_database, name='file'),
    path('distinct/', get_distinct_values, name='distinct'),
    path('query/', query_patient_data, name='query'),
    path('delete/', delete_from_table, name='delete'),
    path('query/extension/', glyph_heatmap_request, name='extension'),
]
