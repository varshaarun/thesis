from rest_framework.response import Response
from rest_framework import status
from .models import PatientData
from .serializers import PatientDataSerializer
from .filters import PatientFilter
from rest_framework.decorators import api_view
from .read_data_from_csv import read_csv
from rest_framework.exceptions import ParseError
from .queries import distinct_values, query_parser
from .glyph_heatmap_extension import request_parser


# get all data from the database
@api_view(['GET'])
def list_all_patients(request):
    try:
        patient = PatientData.objects.all()
        # print(PatientData.objects.all())
        serializer = PatientDataSerializer(patient, many=True)
        return Response(serializer.data)
    except Exception as e:
        Response({'status': status.HTTP_500_INTERNAL_SERVER_ERROR, 'message': e})


# add patient details to the database
@api_view(['POST'])
def add_patients_to_database(request):
    try:
        patient_data = request.data.get('file')
        # print(request.data)
        # file_name = patient_data.name.split('.')[0]
        patient_dict = read_csv(patient_data)
        PatientData.objects.all().delete()
        for patient in patient_dict:
            patient_serializer = PatientDataSerializer(data=patient)
            if patient_serializer.is_valid():
                patient_serializer.save()
            if not patient_serializer.is_valid():
                print(patient_serializer.errors)
        return Response({'status': status.HTTP_201_CREATED, 'message': 'Patient details added to the database'},
                        status=status.HTTP_201_CREATED)
    except Exception as e:
        raise ParseError({'file_name': request.data.get, 'message': e})


# delete all data from database
@api_view(['DELETE'])
def delete_from_table(request):
    try:
        delete_patients = PatientData.objects.all().delete()
        return Response({'status': status.HTTP_200_OK, 'message': 'Patient details deleted to the database'},
                        status=status.HTTP_200_OK)
    except Exception as e:
        Response({'status': status.HTTP_500_INTERNAL_SERVER_ERROR, 'message': e})


# get distinct values from the csv for filters
@api_view(['GET'])
def get_distinct_values(request):
    values = distinct_values()
    return Response(values)


# filter query
@api_view(['POST'])
def query_patient_data(request):
    try:
        patients = query_parser(request.data)
        return Response(patients, status=status.HTTP_200_OK)
    except Exception as e:
        Response({'status': status.HTTP_500_INTERNAL_SERVER_ERROR, 'message': e})


# glyph/heatmap extension request
@api_view(['POST'])
def glyph_heatmap_request(request):
    try:
        patients = request_parser(request.data)
        return Response(patients, status=status.HTTP_200_OK)
    except Exception as e:
        Response({'status': status.HTTP_500_INTERNAL_SERVER_ERROR, 'message': e})
