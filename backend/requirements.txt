Django==3.1.7
django-cors-headers==3.7.0
djangorestframework==3.12.2
pandas==0.24.2
django-filter==2.4.0
numpy==1.14.5
sqlparse==0.4.1
python-dateutil==2.8.0