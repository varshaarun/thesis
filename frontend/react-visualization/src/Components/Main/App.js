import './App.css';
import {getFilters} from "../../Service/httpClient";

import React, {Component} from 'react';
import Header from "./Header";
import Visualization from "./Visualization";

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            filterData: null
        }
    }


    async componentDidMount() {
        let filterResponse = await getFilters();
        this.changeState("filterData", filterResponse);
    }

    changeState = (stateName, stateValue) => {
        this.setState({[stateName]: stateValue})
    }

    render() {
        return (
            <div id={"main-container"}>
                {/*calls header component and sends this.changeState*/}
                <Header updateFilterData={this.changeState}/>
                {this.state.filterData !== null && <Visualization filterData={this.state.filterData}/>}
            </div>
        );
    }
}

export default App;
