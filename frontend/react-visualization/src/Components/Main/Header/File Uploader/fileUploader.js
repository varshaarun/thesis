import React, {useEffect, useMemo} from 'react';
import ReactDropZone, { useDropzone } from 'react-dropzone';
import {Button, Dialog, DialogTitle, List, ListItem, ListItemIcon, ListItemText} from "@material-ui/core";
import DeleteIcon from '@material-ui/icons/Delete';
import "./index.css"

const baseStyle = {
    display: 'flex',
    alignItems: 'center',
    padding: '20px',
    borderWidth: 2,
    borderRadius: 2,
    borderColor: '#eeeeee',
    borderStyle: 'dashed',
    backgroundColor: '#fafafa',
    color: '#bdbdbd',
    outline: 'none',
    transition: 'border .24s ease-in-out',
    margin: '10px',
    flexDirection: "row",
    width: "50%"
};

function FileUploader(props) {


    const changeUploadedFile = (file) => {
        props.changeUploadedFiles(file);
    }

    const renderUploadedFiles = () => props.uploadedFiles.map((file, index) =>
        <ListItem key={file.name} style={{display: "flex"}}>
            <ListItemIcon onClick={() => changeUploadedFile([])}><DeleteIcon color={"secondary"} cursor= {"pointer"}/></ListItemIcon>
            <ListItemText primary={file.name} secondary={`${file.size} bytes`}/>
        </ListItem>
    );

    return (
        <Dialog open maxWidth={"md"} fullWidth>
            <DialogTitle>Upload Panel</DialogTitle>
            <div className={"container"}>
                <ReactDropZone onDrop={(files) => changeUploadedFile([files[0]])}>
                    {({getRootProps,getInputProps, isDragActive, isDragReject}) => (
                        <div {...getRootProps({style:baseStyle})}>
                            <input {...getInputProps()} accept={".CSV"}/>
                            <p>Drag 'n' drop some files here, or click to select files</p>
                            {isDragReject && "Unsupported file format"}
                        </div>
                    )}
                </ReactDropZone>
                <div>
                    <h4 style={{alignSelf: "center"}}>Uploaded files</h4>
                    {props.uploadedFiles.length === 0 && <p>No files are uploaded</p>}
                    {renderUploadedFiles()}
                </div>
            </div>
            <div style={{alignSelf: "center"}}>
                <Button style={{margin:"10px"}} disabled={!props.uploadedFiles.length}    variant="contained" color="primary" onClick={() => {props.uploadFile(); props.closeUploadPanel()}}>Upload</Button>
                <Button style={{margin:"10px"}} variant="contained" color="primary" onClick={props.closeUploadPanel}> Close </Button>
            </div>
        </Dialog>
    );
}

export default FileUploader;