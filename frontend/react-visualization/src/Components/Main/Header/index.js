import React, {Component} from 'react';
import OVGULogo from "../../../Assets/Images/OVGULogo.jpg";
import "./index.css"
import BackupIcon from '@material-ui/icons/Backup';
import {IconButton, ListItem, ListItemIcon, ListItemText, Popover} from "@material-ui/core";
import AppsIcon from '@material-ui/icons/Apps';
import FileUploader from "./File Uploader/fileUploader";
import {fileUploadRequest, getFilters} from "../../../Service/httpClient";

class Header extends Component {
    // when header component is called, whatever is there inside constructor gets initialised
    constructor(props) {
        super(props);
        this.state = {
            userOptions: null,
            openDialogBox: false,
            //uploadState: false,
            uploadedFiles: []
        }
        this.userOptions = [
            { label: "File Uploader", icon: <BackupIcon color={"primary"}/>, onclick: () => this.changeState("openDialogBox", !this.state.openDialogBox)}
        ]
    }

    changeState = (stateName, stateValue) => {
        this.setState({[stateName]: stateValue})
    }


    uploadFile = async () => {
        try {
            let requestBody = new FormData();
            requestBody.append("file", this.state.uploadedFiles[0]);
            // requestBody - file buffer which was uploaded by the user
            await fileUploadRequest(requestBody);
            let filterResponse = await getFilters();
            this.props.updateFilterData("filterData", filterResponse)
        }
        catch (e) {
            console.log("Error")
        }
    }
    // after initialization, render() is called
    render() {
        return (
            // main division for the header
            <div id={"header"}>
                <img id={"logo"} src={OVGULogo} alt={"OVGU-FIN-Logo"}/>
                <div id={"app-name"}>Aneurysm Visualizer</div>
                <div id={"user-options"}>
                    {/*icon on the right top which has option like file upload. onclick takes the current state i.e., if clicked, popover opens, if again clicked popover closes*/}
                    <IconButton color={"primary"} onClick={(event) => this.changeState("userOptions", event.currentTarget)}>
                        <AppsIcon fontSize={"large"}/>
                    </IconButton>
                    {/*Popover contains list of options like file upload etc. when popover is closed, useroption state is set to null, open=(open popover only when useroptions is not null) */}
                    <Popover open={this.state.userOptions !== null} onClose={() => this.changeState("userOptions", null)}
                             anchorEl={this.state.userOptions}
                             anchorOrigin={{
                                 vertical: 'bottom',
                                 horizontal: 'center',
                             }}
                             transformOrigin={{
                                 vertical: 'top',
                                 horizontal: 'center',
                             }}
                    >
                        {/*maps through the list of useroptions and displays on the popover*/}
                        {this.userOptions.map(content => {
                            return <ListItem button onClick={() => {content.onclick();this.changeState("userOptions", null)}} key={content.label}>
                                <ListItemIcon >{content.icon}</ListItemIcon>
                                <ListItemText>{content.label}</ListItemText>
                            </ListItem>
                        })}
                    </Popover>
                </div>
                {/*if openDialogBox is true, i.e., user has clicked file upload open, open the file dialog box to upload file*/}
                {this.state.openDialogBox &&
                <FileUploader
                    uploadedFiles={this.state.uploadedFiles}
                    changeUploadedFiles={(file) => this.changeState("uploadedFiles", file)}
                    closeUploadPanel={() => this.changeState("openDialogBox", false)}
                    uploadFile={this.uploadFile}

                />
                }
            </div>
        )
    }
}

export default Header;