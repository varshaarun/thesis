import React, {Component} from 'react';
import {
    Button,
    Checkbox,
    FormControlLabel,
    FormGroup,
    Popover
} from "@material-ui/core";
import ArrowDropDownCircleIcon from '@material-ui/icons/ArrowDropDownCircle';
import "./index.css"

class AneurysmLocation extends Component {

    constructor(props) {
        super(props);
        this.state = {
            aneurysmLocation: this.props.aneurysmLocation,
            aneurysmLocationPopover: null,
        }
    }

    changeState = (stateName, stateValue) => {
        this.setState({[stateName]: stateValue})
    }

    modifyAneurysmLocation = (location) => {
        const aneurysmLocationCopy = [...this.state.aneurysmLocation];
        if(this.state.aneurysmLocation.indexOf(location) === -1) {
            aneurysmLocationCopy.push(location);
        }
        else {
            aneurysmLocationCopy.splice(aneurysmLocationCopy.indexOf(location), 1);
        }
        this.setState({aneurysmLocation: aneurysmLocationCopy});
        this.props.modifyFilter("aneurysmLocation", aneurysmLocationCopy);
    }

    getPreferredAneurysmLocation = () => {
        return (
            <>
                <Button endIcon={<ArrowDropDownCircleIcon/>} color={"primary"} variant={"contained"} style={{marginRight: "10px"}}
                        onClick={(e) => this.changeState("aneurysmLocationPopover", e.currentTarget)}>
                    Locations
                </Button>
                <Popover open={this.state.aneurysmLocationPopover !== null} onClose={() => this.changeState("aneurysmLocationPopover", null)}
                         anchorEl={this.state.aneurysmLocationPopover} style={{minWidth: "150px"}}
                         anchorOrigin={{
                             vertical: 'bottom',
                             horizontal: 'center',
                         }}
                         transformOrigin={{
                             vertical: 'top',
                             horizontal: 'center',
                         }}
                >
                    <FormGroup row id={"aneurysm-group"}>
                        {this.props.aneurysmLocation.map(location =>  { if(location !== 'other' && location !== 'NA'){
                            return(<FormControlLabel key={location}
                                              control={<Checkbox color={"primary"} checked={this.state.aneurysmLocation.indexOf(location) !== -1}/>}
                                              name={location}
                                              onChange={(e) => this.modifyAneurysmLocation(e.target.name)}
                                              label={location}/>)}}
                        )}
                    </FormGroup>
                </Popover>
            </>
        )
    }

    render() {
        return (
            <div id={"aneurysm-container"}>
                {this.getPreferredAneurysmLocation()}
                {/*<div id={"visualize"}>*/}
                {/*    <Button color={"primary"} variant={"contained"} onClick={this.props.filterQuery}>*/}
                {/*        Visualize*/}
                {/*    </Button>*/}
                {/*</div>*/}
                <Button color={"primary"} variant={"contained"} onClick={this.props.filterQuery}>
                    Visualize
                </Button>
            </div>
        );
    }
}

export default AneurysmLocation;