import React, {Component, createRef} from 'react';
import {Card, CardContent, CardHeader, Typography} from "@material-ui/core";
import PropTypes from "prop-types";

class CowTooltip extends Component {

    constructor(props) {
        super(props);
        this.cardRef = createRef();
        this.tooltipCardOffsetY = 30;
        this.tooltipCardOffsetX = 10;
        this.state = {
            tooltipPositionX: 0,
            tooltipPositionY: 0,
        };
    }


    //if position x and z is calculated in render, at the first execution, cardRef would be null.
    // cardRef is used to access all  the details of the card(screen height)
    componentDidMount() {
        const {pageX, pageY, anchorEl} = this.props;
        const {screenX, screenY} = anchorEl;
        const {innerHeight, innerWidth} = anchorEl.view;
        const tooltipCardHeight = this.cardRef.current.clientHeight;
        const tooltipCardWidth = this.cardRef.current.clientWidth;
        //Calculate x and y coordinates
        const tooltipPositionX = (screenX + this.tooltipCardOffsetX + tooltipCardWidth) > innerWidth ?
            pageX - (this.tooltipCardOffsetX + tooltipCardWidth) : pageX + this.tooltipCardOffsetX;

        const tooltipPositionY = (screenY + this.tooltipCardOffsetY + tooltipCardHeight) > innerHeight ?
            pageY - (this.tooltipCardOffsetY + tooltipCardHeight) : pageY + this.tooltipCardOffsetY

        this.setState({tooltipPositionX, tooltipPositionY})
    }


    render() {
        const {locationName, glyphMax, glyphCurrentValue, glyphPercentage, showGlyph, showHeatmap,
            heatmapMax, heatmapCurrentValue, heatmapPercentage, skeletonColor} = this.props;
        const {r = 0, g = 0, b = 0, a = 1} = skeletonColor;
        const brightness = Math.round(((r * 299) + (g * 587) + (b * 114)) / 1000)
        const tooltipHeaderColor = brightness > 125 || a < 0.3 ? "black" : "white";
        return (
            //<div id={"COW-tooltip"} style={{maxWidth: "200px"}}>
            <Card
                style={{zIndex: 2, maxWidth: "300px", position: "absolute", top: this.state.tooltipPositionY, left: this.state.tooltipPositionX}}
                ref={this.cardRef}>
                <CardHeader title={locationName}
                            style={{background: `rgba(${r},${g},${b},${a})`, color: tooltipHeaderColor}}
                />
                {showGlyph && <CardContent className={"text-align-left"}>
                    <Typography gutterBottom variant="h6" component="h2"> Glyph </Typography>
                    <Typography variant={"body1"} color={"textSecondary"}>
                        No. of patients: {glyphCurrentValue} <br/>
                        Max no. of patients w.r.t type: {glyphMax}<br/>
                        {/*Percentage: {`${glyphPercentage} %`}*/}
                    </Typography>
                </CardContent>}
                {showGlyph && showHeatmap && <hr/>}
                {showHeatmap && <CardContent className={"text-align-left"}>
                    <Typography gutterBottom variant="h6" component="h2"> Heatmap </Typography>
                    <Typography variant={"body1"} color={"textSecondary"}>
                        No. of patients: {heatmapCurrentValue} <br/>
                        Max no. of patients w.r.t type: {heatmapMax}<br/>
                        {/*Percentage: {`${heatmapPercentage} %`}*/}
                    </Typography>
                </CardContent>}
            </Card>
            //</div>
        );
    }
}

CowTooltip.proptype = {
    locationName: PropTypes.string.isRequired,
    glyphMax: PropTypes.number,
    glyphCurrentValue: PropTypes.oneOfType([PropTypes.number || PropTypes.string]).isRequired,
    glyphPercentage: PropTypes.number.isRequired,
    heatmapMax: PropTypes.number,
    heatmapCurrentValue:  PropTypes.oneOfType([PropTypes.number || PropTypes.string]).isRequired,
    heatmapPercentage: PropTypes.number.isRequired,
    pageX: PropTypes.number.isRequired,
    pageY: PropTypes.number.isRequired
}



export default CowTooltip;