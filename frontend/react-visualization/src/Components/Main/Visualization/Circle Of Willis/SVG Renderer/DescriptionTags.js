import React from "react";

const DescriptionTags = () => <>
    <text id="Pericall-right-description" x="137.64413" y="60.103146">
        <tspan id="tspan1676" x="137.64413" y="60.103146">Pericall right</tspan>
    </text>
    <text id="Pericall-left-description" x="53.949051" y="58.599133">
        <tspan id="tspan1676-6" x="53.949051" y="58.599133">Pericall left</tspan>
    </text>
    <text id="PICA-left-description" x="51.5979" y="262.4043">
        <tspan id="tspan1676-6-5" x="51.5979" y="262.4043">PICA left</tspan>
    </text>
    <text id="AICA-left-description" x="56.804279" y="197.88803">
        <tspan id="tspan1676-6-2" x="56.804279" y="197.88803">AICA left</tspan>
    </text>
    <text id="BasilarTip-description" x="97.276878" y="133.03249">
        <tspan id="tspan1676-6-7" x="97.276878" y="133.03249">BasilarTip</tspan>
    </text>
    <text id="PCA-left-description" x="55.588207" y="161.81203">
        <tspan id="tspan1676-6-58" x="55.588207" y="161.81203">PCA left</tspan>
    </text>
    <text id="PCOM-left-description" x="67.032715" y="139.73557">
        <tspan id="tspan1676-6-8" x="67.032715" y="139.73557">PCOM left</tspan>
    </text>
    <text id="M3-left-description" x="28.460348" y="150.55461">
        <tspan id="tspan1676-6-0" x="28.460348" y="150.55461">M3 left</tspan>
    </text>
    <text id="AchoA-left-description" x="56.857468" y="113.35606">
        <tspan id="tspan1676-6-22" x="56.857468" y="113.35606">AchoA</tspan>
        <tspan id="tspan2080" x="56.857468" y="117.76578"> left</tspan>
    </text>
    <text id="M2-left-description" x="1.7227332" y="102.81767">
        <tspan id="tspan1676-6-9" x="1.7227332" y="102.81767">M2 left</tspan>
    </text>
    <text id="MCA-BIF-left-description" x="12.656861" y="86.391464">
        <tspan id="tspan1676-6-4" x="12.656861" y="86.391464">MCA-BIF</tspan>
        <tspan id="tspan2283" x="12.656861" y="90.801186">left</tspan>
    </text>
    <text id="M1-left-description" x="41.00079" y="76.25071">
        <tspan id="tspan1676-6-51" x="41.00079" y="76.25071">M1 left</tspan>
    </text>
    <text id="CarotidT-left-description" x="61.551201" y="75.885147">
        <tspan id="tspan1676-6-6" x="61.551201" y="75.885147">CarotidT</tspan>
        <tspan id="tspan2339" x="61.551201" y="80.294868">left</tspan>
    </text>
    <text id="Acom-description" x="102.11184" y="65.605354">
        <tspan id="tspan1676-6-50" x="102.11184" y="65.605354" dx="0 0 0" dy="0 0 0"
               rotate="0 0 0 0">Acom
        </tspan>
    </text>
    <text id="AnteriorSpinalArtery-description" x="103.83134" y="271.9458">
        <tspan id="tspan1676-6-5-6" x="93.83134" y="271.9458">Anterior Spinal</tspan>
        <tspan id="tspan1908" x="100.83134" y="276.35553">Artery</tspan>
    </text>
    <text id="PICA-right-description" x="138.03287" y="262.68494">
        <tspan id="tspan1676-6-5-3" x="138.03287" y="262.68494">PICA right</tspan>
    </text>
    <text id="MCA-BIF-right-description" x="188.10756" y="87.483543">
        <tspan id="tspan1676-6-5-3-9" x="188.10756" y="87.483543">MCA-BIF</tspan>
        <tspan id="tspan2281" x="188.10756" y="91.893265">right</tspan>
    </text>
    <text id="M3-right-description" x="166.27959" y="148.73457">
        <tspan id="tspan1676-6-5-3-3" x="166.27959" y="148.73457">M3 right</tspan>
    </text>
    <text id="AchoA-right-description" x="145.3243" y="112.20004">
        <tspan id="tspan1676-6-5-3-97" x="145.3243" y="112.20004">AchoA</tspan>
        <tspan id="tspan2078" x="145.3243" y="116.60976">right</tspan>
    </text>
    <text id="ACI-right-description" x="111.82047" y="122.62823">
        <tspan id="tspan1676-6-5-3-32" x="111.82047" y="122.62823">ACI right</tspan>
    </text>
    <text id="ACI-left-description" x="88.66082" y="122.90887">
        <tspan id="tspan1676-6-5-3-6" x="88.66082" y="122.90887">ACI left</tspan>
    </text>
    <text id="PCOM-right-description" x="129.5153" y="139.79561">
        <tspan id="tspan1676-6-5-3-2" x="129.5153" y="139.79561">PCOM right</tspan>
    </text>
    <text id="PCA-right-description" x="145.01155" y="164.02359">
        <tspan id="tspan1676-6-5-3-96" x="145.01155" y="164.02359">PCA right</tspan>
    </text>
    <text id="AICA-right-description" x="139.86938" y="198.38647">
        <tspan id="tspan1676-6-5-3-4" x="139.86938" y="198.38647">AICA right</tspan>
    </text>
    <text id="M2-right-description" x="192.99492" y="102">
        <tspan id="tspan1676-6-5-3-9-4" x="192.99492" y="102">M2 right</tspan>
    </text>
    <text id="M1-right-description" x="161.37712" y="79.027908">
        <tspan id="tspan1676-6-5-3-9-1" x="161.37712" y="79.027908">M1 right</tspan>
    </text>
    <text id="CarotidT-right-description" x="139.8911" y="81.425606">
        <tspan id="tspan1676-6-5-3-9-0" x="139.8911" y="81.425606">CarotidT</tspan>
        <tspan id="tspan2391" x="144.8911" y="85.835327">right</tspan>
    </text>
    <text id="ophthal-right-description" x="112.12719" y="100.20213">
        <tspan id="tspan1676-6-51-4-2" x="112.12719" y="100.20213">ophthal</tspan>
        <tspan id="tspan2238" x="112.12719" y="104.61185">right</tspan>
    </text>
    <text id="ophthal-left-description" x="93.326302" y="99.867035">
        <tspan id="tspan1676-6-51-4-2-4" x="93.326302" y="99.867035">ophthal</tspan>
        <tspan id="tspan2238-4" x="93.326302" y="104.27676">left</tspan>
    </text>
    <text id="A1-left-description" x="82.79216" y="69.626579">
        <tspan id="tspan2339-6" x="82.79216" y="69.626579">A1</tspan>
        <tspan id="tspan2437" x="82.79216" y="74.036301">left</tspan>
    </text>
    <text id="A1-right-description" x="128.03583" y="70.781715">
        <tspan id="tspan2339-6-7" x="128.03583" y="70.781715">A1</tspan>
        <tspan id="tspan2439" x="128.03583" y="75.191437">right</tspan>
    </text>
</>

export default DescriptionTags;