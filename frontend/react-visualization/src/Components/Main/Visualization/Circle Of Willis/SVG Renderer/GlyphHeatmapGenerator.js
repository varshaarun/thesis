import React from "react";
import PropTypes from "prop-types"

const GlyphHeatmapGenerator = (props) => {

    const { locationName, cx, cy, showGlyph, showHeatmap, strokeColor,
        fillColor, glyphPercentage,showCowTooltip, hideCowTooltip, handleClick } = props;
    const glyphRadius = 3.9;
    const heatmapRadius = 3
    const glyphPercentageFontSize = 0.85;
    const {r = 0, g = 0, b = 0, a = 1} = fillColor;
    const {r: scr, g: scg, b: scb, a: sca} = strokeColor;
    //according w3 specifications
    const brightness = Math.round(((r * 299) + (g * 587) + (b * 114)) / 1000)
    const glyphPercentageColor = brightness > 125 || a < 0.3 ? "black" : "white";


    return (
        <>
            {showHeatmap &&
            <circle id={`heatmap_${locationName}`}
                    className={'cursor-pointer'}
                    fill={`rgba(${r},${g},${b},${a})`} stroke="none"
                    cx={cx} cy={cy} r={heatmapRadius}
            />
            }
            {showGlyph &&
            <>
                <circle id={`glyphLoader_${locationName}`}
                        cx={cx} cy={cy} r={glyphRadius} fill="none"
                         stroke={`rgba(${scr},${scg},${scb},${0.1})`}
                        strokeWidth={0.85}
                />
                <circle id={`glyph_${locationName}`}
                        className={'cursor-pointer'}
                        style={{transform: "rotateZ(-90deg)", transformOrigin: `${cx}px ${cy}px`}}
                        fill="none" stroke={`rgba(${scr},${scg},${scb},${sca})`} strokeWidth={0.85}
                        strokeLinecap={"round"} strokeLinejoin={"round"}
                        strokeDasharray={ 2*Math.PI*glyphRadius }
                        strokeDashoffset={ 2*Math.PI*glyphRadius * (1 - (glyphPercentage/100)) }
                        cx={cx} cy={cy} r={glyphRadius}/>
                {/*<text id={`glyph_${locationName}`}*/}
                {/*      className={'cursor-pointer'}*/}
                {/*      style={{ fill: glyphPercentageColor, stroke: glyphPercentageColor, strokeWidth: 0.05, fontSize: `${glyphPercentageFontSize}mm`}}*/}
                {/*      x={Number(cx) - (String(glyphPercentage).length * glyphPercentageFontSize)}*/}
                {/*      y={Number(cy) + glyphPercentageFontSize}*/}
                {/*>*/}
                {/*    {glyphPercentage}*/}
            </>
            }
            {(showHeatmap || showGlyph) &&
            <circle id={`hoverCircle_${locationName}`}
                    className={'cursor-pointer'}
                    values={locationName}
                    fill={"transparent"}
                    cx={cx} cy={cy} r={1.1 * glyphRadius} stroke="none"
                    onMouseEnter={(e) => showCowTooltip(e.pageX, e.pageY, e)}
                    onMouseLeave={hideCowTooltip}
                    onClick={handleClick}
            />}
        </>
    );
};

GlyphHeatmapGenerator.propTypes = {
    locationName: PropTypes.string.isRequired,
    cx: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    cy: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    showGlyph: PropTypes.bool,
    showHeatmap: PropTypes.bool,
    strokeColor: PropTypes.object.isRequired,
    fillColor: PropTypes.object.isRequired,
    glyphPercentage: PropTypes.number.isRequired
}

GlyphHeatmapGenerator.defaultProps = {
    showGlyph: true,
    showHeatmap: true
}

export default GlyphHeatmapGenerator;


