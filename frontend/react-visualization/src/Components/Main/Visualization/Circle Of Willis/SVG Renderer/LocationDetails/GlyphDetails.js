import React, {Component, createRef} from 'react';
import {Chart, registerables} from "chart.js";
import ColorScale from "color-scales";
import {GLYPH_DETAILS_COLOR_PALETTES} from "../../../../../../Helper/Constants/glyphDetailColors";
import {Grid} from "@material-ui/core";

class GlyphDetails extends Component {

    constructor(props) {
        super(props);
        this.canvasRef = createRef();
        Chart.register(...registerables);
        this.CHART_OBJECT = null;
    }

    shouldRenderChart = Boolean(this.props.overAllFilterResult.length)

    componentDidMount() {
        this.shouldRenderChart &&
        this.renderChart();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        this.shouldRenderChart && this.renderChart();
    }

    chartOnClick = (legendItem) => {
        this.CHART_OBJECT.getDatasetMeta(
            legendItem.datasetIndex
        ).hidden = !this.CHART_OBJECT.getDatasetMeta(
            legendItem.datasetIndex
        ).hidden;
        this.CHART_OBJECT.update();
    }

    onClickHandler = (text, datasetIndex) => {
        this.chartOnClick( {text, datasetIndex})
    }

    renderChart = () => {
        const drawingContext = this.canvasRef.current.getContext("2d");

        if(this.CHART_OBJECT) this.CHART_OBJECT.destroy();

        this.CHART_OBJECT = new Chart(drawingContext, {
            type: 'doughnut',
            data: this.chartData(),
            options: {
                responsive: true,
                plugins: {
                    legend: {
                        labels: {
                            generateLabels: (chart) => []//{
                        },
                        onClick: this.chartOnClick
                    },
                }
            },
        })
    }

    getGlyphColor = (max, colorIndex, currentValue) => {
        return new ColorScale(0, max, GLYPH_DETAILS_COLOR_PALETTES[colorIndex],0.95).getColor(currentValue).toHexString();
    }

    chartData = () => {
        const {overAllFilterResult, individualFilterResult, count} = this.props;
        const {max} = count
        const labels = [], datasets = [];
        let colorPaletteIndex = 0;
        //glyph overall
        labels.push("Overall", "unfilled");
        datasets.push({
            backgroundColor: [this.getGlyphColor(overAllFilterResult.length, colorPaletteIndex, overAllFilterResult.length), "#cdcdcd"],
            data:[overAllFilterResult.length, max - overAllFilterResult.length]
        });

        //glyph individual
        individualFilterResult.forEach(filter => {
            colorPaletteIndex = (colorPaletteIndex + 1) % GLYPH_DETAILS_COLOR_PALETTES.length;
            const data = [], background = [], labelCount = labels.length;
            let totalCount = 0, glyphMax = 0;

            filter.value.forEach((value) => {
                glyphMax = glyphMax < value.data.length ? value.data.length : glyphMax
            })

            filter.value.forEach((value, index) => {
                if(value.data.length !== 0) {
                    labels.push(value.id);
                    data.push(value.data.length);
                    background.push(this.getGlyphColor(glyphMax ,colorPaletteIndex, value.data.length));
                    totalCount += value.data.length;
                }
            })

            if(max - totalCount !== 0) {
                labels.push(`unfilled_${filter.key}`);
                data.push(max - totalCount);
                background.push("#cdcdcd")
            }

            datasets.push({
                backgroundColor: [...new Array(labelCount).fill("#cdcdcd"),...background],
                data: [...new Array(labelCount).fill(0), ...data]
            })
        })
        return {labels, datasets};
    }

    getLegends = () => {
        const {individualFilterResult} = this.props;
        const legends = ["Overall"];
        individualFilterResult.forEach((filter, index) => {
            legends.push(filter.key)
        })
        return legends.map((legend, index) => (
            <Grid item onClick={() => this.onClickHandler(legend, index)} style={{display: 'flex', cursor: 'pointer'}}>
                <div style={{width: 50, height: 15, margin: '4px 2px',
                    background: `linear-gradient(90deg, ${GLYPH_DETAILS_COLOR_PALETTES[index][1]}, ${GLYPH_DETAILS_COLOR_PALETTES[index][0]})`}}/>
                {`${legend.split(/(?=[A-Z])/).join(" ")[0].toUpperCase()}${legend.split(/(?=[A-Z])/).join(" ").slice(1)}`}
            </Grid>
        ))
    }


    render() {
        return (
            this.shouldRenderChart ?
            <Grid style={{width: 600, height: 600}}>
                    <Grid spacing={2} container style={{justifyContent: 'center'}}>{this.getLegends()}</Grid>
                    <canvas height={600} width={600} ref={this.canvasRef}/>
            </Grid>
                :
            <Grid>
                No records available at this location
            </Grid>
        );
    }
}

export default GlyphDetails;