import React, {Component} from 'react';
import {Box, List, ListItem, ListItemText, Typography} from "@material-ui/core";

class HeatmapDetails extends Component {

    generateHeatmapDetails = (currentValue) => {
        const {getHeatmapColor, count} = this.props;
        const {min, max} = count;
        const {r, g, b,a } = getHeatmapColor(min, max, currentValue);
        return <div style={{width: 25, height: 25, borderRadius: "50%",
            background:`rgba(${r},${g},${b},${a})`, display: "flex",
    fontSize: "smaller", alignItems: "center", justifyContent: "center"
        }}>{currentValue}</div>
    }

    generateHeatmapList = (isIndividualFilter) => {
        const {overAllFilterResult, individualFilterResult} = this.props;
        return (
            !isIndividualFilter ?
                <ListItem>
                    <ListItemText primary={"Overall"} style={{flex: "unset", minWidth: 200}}/>
                    <Box component={"span"}>
                        <Typography variant={"caption"} color={"textSecondary"}>Overall</Typography>
                        {this.generateHeatmapDetails(overAllFilterResult.length)}
                    </Box>
                </ListItem>
                :
                individualFilterResult.map(filter => (
                    <ListItem>
                        <ListItemText
                            primary={`${filter.key.split(/(?=[A-Z])/).join(" ")[0].toUpperCase()}${filter.key.split(/(?=[A-Z])/).join(" ").slice(1)}`}
                            style={{flex: "unset", minWidth: 200}}/>
                        <div id={"heatmapDetailsHolder"} style={{display: "flex", flexWrap: "wrap"}}>
                            {filter.value.map(value => (
                                <Box component={"div"}
                                     style={{ margin: 5, display: "flex", flexDirection: "column", alignItems: "center"}}
                                >
                                    <Typography variant={"caption"} color={"textSecondary"}>{value.id}</Typography>
                                    {this.generateHeatmapDetails(value.data.length)}
                                </Box>
                            ))}
                        </div>
                    </ListItem>
                ))
        );}

    render() {
        return (
            <div>
                <List>
                    {this.generateHeatmapList(false)}
                    {this.generateHeatmapList(true)}
                </List>
            </div>
        );
    }
}

export default HeatmapDetails;