import React, {Component} from 'react';
import {Dialog, DialogContent, DialogTitle, FormControl, InputLabel, MenuItem, Select} from "@material-ui/core";
import {GLYPH, HEATMAP, PART_OF} from "../../../../../../Helper/Constants/partOf";
import {getIndividualFilterResult} from "../../../../../../Service/httpClient";
import GlyphDetails from "./GlyphDetails";
import {GLYPH_HEATMAP_TYPE} from "../../../../../../Helper/Constants/types";
import HeatmapDetails from "./HeatmapDetails";

class LocationDetails extends Component {

    constructor(props) {
        super(props);
        this.state = {
            partOf: GLYPH,
            types: "localMaxima",
            loading: true,
            individualFilterResult: null
        }
    }

    componentDidMount() {
        this.fetchIndividualFilterResponse();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(prevState.partOf !== this.state.partOf) {
            this.fetchIndividualFilterResponse();
        }
    }

    changeState = (stateName, stateValue) => {
        this.setState({[stateName]: stateValue});
    }

    getSelect = (label, selectedValue, stateName, options, selectSize) => (
        <FormControl style={{margin: "10px 0"}}>
            <InputLabel>{label}</InputLabel>
            <Select value={selectedValue}
                    style={{width: selectSize}}
                    onChange={(e) => this.changeState( stateName, e.target.value)}>
                {options.map((operation) => (
                    <MenuItem key={`${label}_${operation.label}`} value={operation.value}>
                        {operation.label}
                    </MenuItem>
                ))}
            </Select>
        </FormControl>
    );

    fetchIndividualFilterResponse = async () => {
        this.changeState("loading", true);
        const {appliedFilters, location} = this.props;
        const filterDataDistribution = [];
        const filterData = appliedFilters[this.state.partOf];
        const filterOperator = appliedFilters[`${this.state.partOf}Operator`];
        for (const filter of filterData) {
            const index = filterData.indexOf(filter);
            const requestBody = {...filter, location};
            const filterDataCopy = [...filterData];
            filterDataCopy.splice(index,1);
            const dependsOn = filterDataCopy;
            if(filterOperator === "AND" && dependsOn.length) {
                requestBody.dependsOn = dependsOn;
            }
            getIndividualFilterResult(requestBody).then(response => {
                const valueMapping = [];
                filter.value.forEach(id => {
                    valueMapping.push({id, data: response[id]})
                })
                filterDataDistribution.push({
                    key: filter.key,
                    value: valueMapping
                })
                if(filterData.length === filterDataDistribution.length) {
                    this.changeState("individualFilterResult", filterDataDistribution);
                    this.changeState("loading", false);
                }
            });
        }
    }

    render() {
        const {locationName, closeLocationDetails, count, glyphColor, getHeatmapColor} = this.props;
        const {partOf, loading, individualFilterResult, types} = this.state;
        return (
            <Dialog open={true} onClose={closeLocationDetails} maxWidth={"md"} fullWidth>
                <DialogTitle>{locationName}</DialogTitle>
                <DialogContent >
                    {this.getSelect("Part", partOf, "partOf", PART_OF, 150 )}
                    {this.getSelect("Type", types, "types", GLYPH_HEATMAP_TYPE, 150 )}
                    {!loading && partOf === GLYPH &&
                    <GlyphDetails
                        individualFilterResult={individualFilterResult}
                        overAllFilterResult={this.props[partOf]}
                        count={count[types][partOf]} glyphColor={glyphColor}
                    />
                    }
                    {!loading && partOf === HEATMAP &&
                    <HeatmapDetails
                        individualFilterResult={individualFilterResult}
                        overAllFilterResult={this.props[partOf]}
                        count={count[types][partOf]} getHeatmapColor={getHeatmapColor}
                    />
                    }
                </DialogContent>
            </Dialog>
        );
    }
}

export default LocationDetails;