import React, {Component, createRef} from 'react';
import "./index.css"
import DescriptionTags from "./DescriptionTags";
import GlyphHeatmapGenerator from "./GlyphHeatmapGenerator";
import {PARTS_MAPPING} from "../../../../../Helper/cowTags";
import CowTooltip from "./COWTooltip";
import LocationDetails from "./LocationDetails";

class SVGRenderer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            COWTooltip: null,
            locationDetails: null
        }
    }

    setTooltip = (data) => {
        this.setState({COWTooltip: data ? data : null})
    }

    showLocationDetails = (data) => {
        this.setState({locationDetails: data ? data : null})
    }

    getCircle = () => {
        const locations = Object.keys(PARTS_MAPPING);
        const {getHeatmapColor, filterResults, glyphColor, glyphHeatmapType, skeletonColor, appliedFilters} = this.props;
        const count = filterResults.count[glyphHeatmapType];
        return locations.map(location =>
            ["leftFilter", "rightFilter"].map(filterSide => {
                const locationName = `${location} ${filterSide === 'leftFilter' ? 'left' : 'right'}`;

                //heatmap calculation section
                const {min: heatmapMin, max: heatmapMax} = glyphHeatmapType === "localMaxima" ? count[filterSide].heatmap : count.heatmap;
                const heatmapLocation = filterResults[filterSide]["heatmap"][location];
                const heatmapCurrentValue = (heatmapLocation && JSON.stringify(heatmapLocation) !== "{}") ? heatmapLocation.length : Number.NaN;
                const fillColor = Number.isNaN(heatmapCurrentValue) ? {
                    r: 0,
                    g: 0,
                    b: 0,
                    a: 0
                } : getHeatmapColor(heatmapMin, heatmapMax, heatmapCurrentValue);
                const heatmapPercentage = Math.round(((heatmapCurrentValue - heatmapMin) / (heatmapMax - heatmapMin) * 100) ) ;
                const showHeatmap = !Number.isNaN(heatmapCurrentValue);

                //glyph calculation section
                const {min: glyphMin, max: glyphMax} = glyphHeatmapType === 'localMaxima' ? count[filterSide].glyph : count.glyph;
                const glyphLocation = filterResults[filterSide]["glyph"][location];
                const glyphCurrentValue = (glyphLocation && JSON.stringify(glyphLocation) !== "{}") ? glyphLocation.length : Number.NaN;
                const glyphPercentage = Number.isNaN(glyphCurrentValue) ? 0 : Math.round((glyphCurrentValue / glyphMax) * 100);
                const showGlyph = !Number.isNaN(glyphCurrentValue)

                //tooltip section
                const tooltipData = {
                    locationName, skeletonColor,
                    glyphCurrentValue, glyphPercentage, glyphMax, patientDetailsGlyph: glyphLocation, showGlyph,
                    heatmapCurrentValue, heatmapPercentage, heatmapMax, patientDetailsHeatmap: heatmapLocation, showHeatmap,
                }

                //location data
                const locationDetails = {
                    locationName,
                    glyph: Number.isNaN(glyphCurrentValue) ? glyphCurrentValue : glyphLocation,
                    heatmap:  Number.isNaN(heatmapCurrentValue) ? heatmapCurrentValue : heatmapLocation,
                    closeLocationDetails: () => this.showLocationDetails(null),
                    appliedFilters: appliedFilters[filterSide],
                    location, glyphColor, getHeatmapColor,
                    count: {...filterResults.count, localMaxima: filterResults.count.localMaxima[filterSide]},
                };

                return <GlyphHeatmapGenerator id={`${location}_${filterSide === 'leftFilter' ? 'left' : 'right'}`}
                                              fillColor={fillColor} showHeatmap={showHeatmap}
                                              glyphPercentage={glyphPercentage} showGlyph={showGlyph}
                                              cx={PARTS_MAPPING[location][filterSide].cx}
                                              cy={PARTS_MAPPING[location][filterSide].cy}
                                              strokeColor={glyphColor}
                                              locationName={locationName}
                                              showCowTooltip={(pageX, pageY, anchorEl) => this.setTooltip({...tooltipData, pageX, pageY, anchorEl})}
                                              hideCowTooltip = {() => this.setTooltip(null)}
                                              handleClick = {() => this.showLocationDetails(locationDetails)}
                />
            })
        )
    }

    render() {
        const { displayDescription, skeletonColor } = this.props;
        const {r, g, b, a} = skeletonColor;
        return (
            <div>
                <svg style={{fill: `rgba(${r},${g},${b},${a})`}} id="circle-of-willis-renderer" version="1.1" viewBox="0 25 210 260">
                    <path id="skeleton" d="m138.11 93.41s-0.95162-1.6328-3.2012-3.4454c-3.401-2.6274-14.135-4.4607-12.149-8.3106 0.23085-0.40068 0.83287-2.0877 1.4224-3.3433 0.58886-1.2564 1.4281-2.5954 2.1229-3.2746 0.74272-0.72536 0.51159-1.0613 0.0508-1.156-0.46076-0.09472-1.1519 0.05261-1.3294 0.29466-0.3642 0.49545-0.70478 1.0605-1.1112 1.7899-0.40713 0.7294-0.87937 1.6232-1.5083 2.7759-0.76843 1.4086-1.2342-1.56-1.5841-4.7205-0.0791-3.4304 0.14932-11.102 0.3787-14.155l16.028-14.214-1.8504-3.8349-14.496 13.322-0.10614-19.509s-2.7658-1.4016-3.862-0.42207c0 0 0.37505 32.109 0.39652 35.424 0.0208 3.3143-0.31414 7.1612 0.20533 7.749 1.1549 0.05599-12.309 0.92071-19.255 0.46188-0.89128-2.451 0.17197-36.427-1.064-43.013 0.23387-0.46773-1.6331-1.5148-2.8352 0.01787l0.30724 20.397s-12.991-10.856-13.989-12.12c-1.7538-2.22-2.4172 2.1703-2.4172 2.1703-1.5447 1.1634 16.767 12.973 16.774 14.736-0.11633 1.104 1.7723 12.375-0.58492 16.897 0 0 0-0.78445-3.6949-4.3764 0 0-0.98096 0.13034-1.0389 1.241 0 0 4.0898 2.4707 3.7521 5.0289-0.75074 3.1199-3.3473 4.9764-5.7553 5.6199 0 0-9.2874 1.7725-13.037 5.5159-29.273-5.3174-32.525-3.8037-50.595 14.132-2.0683 1.7036 0.1402 0.25486-6.4324 4.3882-4.1178 2.5896-4.5759 1.0117-5.1011 1.5162-0.52521 0.50454-0.34386 2.605 1.6141 4.0178 2.6922 1.9426 16.838-9.609 16.838-9.609 2.6605-2.1507 0.23249 5.879 3.7581 8.8894 0 0-16.635 11.991-16.136 13.473 0.49918 1.4826 1.1706 1.849 1.8592 3.8251 0.68868 1.9761 9.6919-7.8732 12.288-10.937 6.9826-8.2408 8.3892 7.1161 5.5631 14.538-1.4111 3.706 2.6079 0.88603 3.6323 0.47564 2.1887-0.87688 1.1163-16.346-1.2786-18.475-8.3795-7.4491-1.7512-17.044-0.22001-19.008 12.632-11.08 25.64-3.4032 26.576-3.0441 1.544 1.5019 3.848 2.3929 5.2787 3.7354 0.66297 0.634 1.105 1.3749 1.2493 2.2403 0 0-3.6949 6.9232-2.0786 11.234 0 0 0.46222 2.4821 2.1937 0.13036 0 0-2.6559-2.0894 1.0389-10.841 4.6436 8.3853 4.8476 12.43 7.2252 20.287 6.7401 18.662 4.4698 11.456 8.8981 21.06-15.266 1.3851-21.307 8.8669-21.307 8.8669 0.24471 2.7711 2.857 3.0479 2.857 3.0479 19.185-13.855 28.9-3.9716 28.9-3.9716-14.777-6.835-24.002 9.6992-24.002 9.6992 0.73482 2.9557 2.3669 1.1083 2.3669 1.1083 16.818-20.321 23.185-3.141 23.185-3.141-4.9793 2.3096-9.8776 0.55451-9.8776 0.55451-0.89798 0.27686 0.08161 1.9396 0.08161 1.9396 10.123 0.36918 10.041-1.4782 10.041-1.4782 0.24471 1.9397-0.16315 2.5865-0.16315 2.5865-5.4701-0.36913-10.858 1.8474-10.858 1.8474-1.7144 0.83144 0.24471 1.9397 0.24471 1.9397 3.9181-2.1242 10.45-1.6628 10.45-1.6628v6.466c-8.8988-1.4774-10.368 1.9397-10.368 1.9397-1.2249 1.7551 0.73485 2.032 0.73485 2.032 3.3471-4.1562 10.123-1.5697 10.123-1.5697l-0.16312 3.694c-1.5398-1.8709-3.169-1.3325-4.8161-0.27686-1.031 0.92688-2.0406 1.8797-3.9189 1.8474-0.8164 0.83143-0.08155 1.9397-0.08155 1.9397 2.5966-0.74154 4.2308-1.9534 4.2451-2.032 2.0993-1.6887 3.3593-0.56748 4.5714 0.78526 0.49009 4.1562-0.65327 7.3434-0.65327 7.3434-8.3271-0.36995-10.449 2.678-10.449 2.678-0.32627 1.1091 1.225 1.016 1.225 1.016 2.1222-2.5865 8.8981-2.2165 8.8981-2.2165-5.4693 8.0357-18.532 11.823-18.532 11.823-8.8165 1.9397-10.695 1.5705-10.695 1.5705-2.2038 0.55375-0.89796 2.8634-0.89796 2.8634 2.613 0.9237 9.8783-1.2013 9.8783-1.2013 15.756-3.694 20.736-10.252 20.736-10.252 1.6328 12.192-6.7484 25.574-6.7484 25.574-9.913 16.573-17.179 37.277-17.179 37.277-2.0629 2.4011-8.7974 10.947-9.418 12.335-0.62061 1.3879 0.7302 1.1245 1.3204 1.157 0 0 5.7304-7.1898 7.4416-9.9088 1.9364-3.0956 0.62149 7.0216 0.36479 10.311 0 0 1.85 3.9822 4.7893-0.0817 0 0-0.65328-17.735 11.756-39.718 0 0 8.8172 5.7267 9.4698 13.855l-0.16312 23.831s1.0253 1.1099 2.1222 0.0639l0.38063-23.677s3.0202-11.808 10.068-13.682c0 0 9.3081 17.251 10.533 38.865 0 0 2.5308 3.6948 5.1431 0.36994l-0.65256-8.2209s4.3267 3.1402 6.1226 7.3887c0 0 2.2045 1.7559 1.8775-1.4774 0 0-5.5516-7.3895-8.2455-9.4215 0 0-3.2656-24.57-14.613-38.61 0 0-8.0008-11.823-7.0213-25.955 0 0 6.4496 9.3292 19.267 10.623-0.0815 0.27772 7.1887 0.17001 11.757 2.9556 0 0 2.0406 0.36916 1.0611-3.2333 0 0-3.9188-1.8474-11.021-2.9556-1.5162-0.33108-15.755-1.5389-20.409-12.469 0 0 1.8775-1.2006 8.4903 2.3096 0 0 1.4689 0.0923 0.89796-1.2936 0 0-5.7964-3.4171-9.552-2.5865 0 0-0.571-7.0188-0.32629-7.7587 0 0 3.5919-0.92368 4.8984-0.0923 0 0 2.3676 1.4782 2.5308 0.55375 0 0 0.9795-0.8306-0.73484-1.8474 0 0-5.1431-1.2929-6.6127-0.36917l-0.3263-3.787s5.1431-1.7551 8.0817 0.093c0 0 1.2249-0.093 0.89797-1.386 0 0-3.5919-2.2173-9.2251-0.55452 0.6461-0.14579-0.1381-2.2222 0.32628-5.7268 0 0 1.8782 3.2333 6.1233 1.4782 0 0 1.5512-1.016 3.4288 0.55375 0 0 1.5505 0.36918 1.0611-1.2006 0 0-1.7144-2.6788-4.7353-1.2936 0 0-3.5919 2.3096-5.8779-1.7551l0.24543-2.678s5.143 3.1402 8.0817 1.9396c0 0 0.40857-1.4782-0.0815-1.4782 0 0-2.4492 1.1082-7.5107-1.4782 0 0 7.837-18.104 23.512 3.6948 0 0 2.2038 1.6628 2.9386-0.83058 0 0-8.3271-16.442-24.002-11.454 0 0 14.613-8.2201 30.206 7.5741 2.5358-0.0307 3.174-0.96901 3.3479-2.7702 0 0-11.349-12.563-25.879-11.453 0 0 8.8925-10.969 11.667-22.422 0 0 0.98574-10.276 7.1899-18.219 0 0 2.9436 6.1372 2.1358 9.4029 0 0 1.0397 1.1763 1.6163-0.13034 0 0-0.34629-7.5765-2.7704-10.32 0.66165-5.0566 7.3375-8.1184 14.193-8.4428 6.8558-0.3244 16.596 5.309 14.782 10.666-3.6761 5.889-6.4615 6.1946-9.5009 14.093-1.2122 3.1502 3.5066 12.284 5.4968 17.433 1.2616 3.2638 7.5383-1.2324 4.6003-0.82951 0 0-12.991-28.554 2.873-18.441 6.4479 4.7638 6.116 9.2248 10.046 11.476 3.5008 2.005 0.31906-1.0295 2.3866-5.5089-5.8436-1.1565-5.245-5.795-12.814-10.509-3.8344-2.388-3.1594-2.7608 1.8173-8.3111 1.2249-5.7445 11.18 7.7165 19.912 11.002l1.9666-2.9519c-5.9593-4.0975-7.6889-6.8583-12.783-10.466-14.898-9.5801-31.068-15.317-41.338-11.167zm-22.687 134.65c-7.1844 3.048-9.7152 12.654-9.7152 12.654 0-4.7107-9.3882-13.116-9.3882-13.116l9.5513-16.441c0.65327 1.016 9.552 16.903 9.552 16.903zm2.6402-84.13c-6.6578 0.0478-12.227 3.1799-12.227 3.1799-2.5837-2.5768-11.446-4.1036-11.446-4.1036-11.271-17.63-13.184-34.636-12.622-35.877 5.1137 6.1234 8.3128 4.5723 8.3128 4.5723 5.0215-9.1778-0.523-12.252-0.46221-12.149l-4.1564-2.3509s1.7323-3.396 3.0016-2.8739c0 0 1.3859-3.0042-1.6163-2.4813 0 0-2.8856 1.3058-3.6942 4.4404 0 0-1.6832-1.5352-4.015-3.3009 0 0-0.55173-2.3377 13.408-6.5302 1.1362-0.71806 1.3953-1.5316 2.666-2.4755 0.63608-0.47276 1.2878-2.3584 2.2617-2.1624 1.3496 0.27155 4.781 0.36407 5.4949 0.18939l0.43862 5.5422c0.50157 0.86945 1.3008-0.14409 1.3008-0.14409l0.31913-5.2248 1.9648 0.02999-0.23107 4.9334c0.63678 0.56749 1.0904 0.19997 1.3573-0.13033l0.2304-4.833 1.8288-0.05591c-4.1e-4 0 0.25686 3.5822 0.27834 4.7585 0.35632 0.44607 0.74986 0.6622 1.391 0.13034l-0.18178-5.0831s5.2639-0.50353 6.0131-0.08339c0.8241 2.2875 1.7177 3.5739 1.7177 3.5739 2.2854 1.7114 3.0756 3.2716 6.8262 3.9435 0 0 5.4922 0.5011 7.3841 6.1388 0 0-1.9626 1.9599-2.8871 2.2214 0 0-0.11505-2.7435-3.9246-4.4412 0 0-2.0306-0.43958-1.8474 1.9591 0 0 3.579 1.437 3.579 3.7887l-2.54 1.828s-2.2811 4.3464 0.57743 9.4053c0 0 1.4847 2.1631 7.5042-2.4813-3.8845 9.1122-1.7409 16.996-16.006 36.147z"/>
                    {displayDescription && <DescriptionTags/>}
                    {this.getCircle()}
                </svg>
                {this.state.COWTooltip && <CowTooltip {...this.state.COWTooltip}/>}
                {this.state.locationDetails && <LocationDetails {...this.state.locationDetails}/>}
            </div>
        );
    }
}

export default SVGRenderer;