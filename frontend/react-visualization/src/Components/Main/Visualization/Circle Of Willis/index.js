import React, {Component} from 'react';
import SVGRenderer from "./SVG Renderer";
import {ChromePicker, HuePicker} from "react-color";
import {
    FormControl,
    FormControlLabel,
    InputLabel,
    MenuItem,
    Popover,
    Radio,
    RadioGroup,
    Select,
    Switch
} from "@material-ui/core";
import "./index.css"
import ColorScale from "color-scales";
import {HEATMAP_COLOR_COLLECTION} from "../../../../Helper/heatmapColor";
import {GLYPH_HEATMAP_TYPE} from "../../../../Helper/Constants/types";

class CircleOfWillis extends Component {

    constructor(props) {
        super(props);
        this.state = {
            heatmapColor: 0,//"rgba(224, 32, 32, 1)",
            displayDescription: true,
            skeletonColor: {r: 0, g: 104, b: 180, a: 1},
            glyphColor: {a: 1, b: 255, g: 42, r: 0},
            colorSelectorPopover: null,
            glyphHeatmapType: "overall"
        };
    }

    changeState = (stateName, stateValue) => {
        this.setState({[stateName]: stateValue});
    }

    getColorString = (color) => {
        const {r, g, b, a} = color;
        return `rgba(${r},${g},${b},${a})`;
    };

    getHeatmapColor = (minValue, maxValue, currentValue) => {
        return new ColorScale(minValue, maxValue, HEATMAP_COLOR_COLLECTION[this.state.heatmapColor],0.95).getColor(currentValue)
    };

    popoverContainer = () => (
        this.state.colorSelectorPopover !== null &&
        <Popover id={"color-picker"} open={true} onClose={() => this.changeState("colorSelectorPopover", null)}
                 anchorEl={this.state.colorSelectorPopover} style={{minWidth: "150px"}}
                 anchorOrigin={{
                     vertical: 'bottom',
                     horizontal: 'center',
                 }}
                 transformOrigin={{
                     vertical: 'top',
                     horizontal: 'center',
                 }}>
            {this.state.colorSelectorPopover.id === "heatmap-color-picker" &&
            <RadioGroup value={this.state.heatmapColor} onChange={(event, value) => this.changeState("heatmapColor", Number(value))}>
                {HEATMAP_COLOR_COLLECTION.map((colorSet, index) => (
                    <div style={{display: "flex", alignItems: "center", margin: "0 10px"}} key={`${colorSet}-gradiant`}>
                        <FormControlLabel  value={index} control={<Radio color={"primary"}/>} label=''/>
                        <div id={"heatmap-color-options"} style={{background: `linear-gradient(90deg, ${colorSet[0]}, ${colorSet[1]})`}}/>
                    </div>
                ))}
            </RadioGroup>
            }
            {this.state.colorSelectorPopover.id === "skeleton-color-picker" &&
            <ChromePicker color={this.state.skeletonColor} onChange={(color) => this.changeState("skeletonColor", color.rgb)}/>}
            {this.state.colorSelectorPopover.id === "glyph-color-picker" &&
            <HuePicker color={this.state.glyphColor} onChange={(color) => this.changeState("glyphColor", color.rgb)}/>}
        </Popover>
    );

    render() {
        return (
            <div id={"circle-of-willis"}>
                <div id={"circle-of-willis-user-options"}>
                    <div id={"color-picker-container"}>Heatmap Color:
                        <div id={"heatmap-color-picker"} style={{background: `linear-gradient(90deg, ${HEATMAP_COLOR_COLLECTION[this.state.heatmapColor][0]}, ${HEATMAP_COLOR_COLLECTION[this.state.heatmapColor][1]})`}}
                             onClick={(event) => this.changeState("colorSelectorPopover", event.currentTarget)}/>
                    </div>
                    <FormControlLabel control={<Switch color={"primary"} checked={this.state.displayDescription} onChange={() => this.changeState("displayDescription", !this.state.displayDescription)}/>}
                                      label={"Display description"}/>
                    <div id={"color-picker-container"}>Circle Of Willis Color:
                        <div id={"skeleton-color-picker"} style={{background: this.getColorString(this.state.skeletonColor)}} onClick={(event) => this.changeState("colorSelectorPopover", event.currentTarget)}/>
                    </div>
                    <div id={"color-picker-container"}>Glyph Color:
                        <div id={"glyph-color-picker"} style={{borderColor: this.getColorString(this.state.glyphColor)}} onClick={(event) => this.changeState("colorSelectorPopover", event.currentTarget)}/>
                    </div>
                    {this.popoverContainer()}
                    <FormControl>
                        <InputLabel>Type</InputLabel>
                        <Select value={this.state.glyphHeatmapType}
                                onChange={(e) => this.changeState("glyphHeatmapType", e.target.value)}>
                            {GLYPH_HEATMAP_TYPE.map(type => (
                                <MenuItem key={type.value} value={type.value}> {type.label} </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                </div>
                <div style={{display: "flex", justifyContent: "center"}}>
                    <div id={"heatmap-gradient"}>
                        <div id={"heatmap-gradient-ruler"}>
                            <span id={"span"} style={{marginTop:"-7px",marginLeft:"-35px"}}>100%</span>
                            <span id={"span"} style={{marginTop:"58px",marginLeft:"-30px"}}>80%</span>
                            <span id={"span"} style={{marginTop:"58px",marginLeft:"-30px"}}>60%</span>
                            <span id={"span"} style={{marginTop:"58px",marginLeft:"-30px"}}>40%</span>
                            <span id={"span"} style={{marginTop:"58px",marginLeft:"-30px"}}>20%</span>
                            <span id={"span"} style={{marginTop:"58px",marginLeft:"-22px"}}>0%</span>
                        </div>
                        <div id={"heatmap-gradient-color"} style={{"background": `linear-gradient(360deg, ${HEATMAP_COLOR_COLLECTION[this.state.heatmapColor][0]}, ${HEATMAP_COLOR_COLLECTION[this.state.heatmapColor][1]})`}}/>
                    </div>
                    <SVGRenderer displayDescription={this.state.displayDescription}
                                 skeletonColor={this.state.skeletonColor}
                                 getHeatmapColor={this.getHeatmapColor}
                                 filterResults={this.props.filterResults}
                                 glyphColor={this.state.glyphColor}
                                 glyphHeatmapType={this.state.glyphHeatmapType}
                                 appliedFilters={this.props.appliedFilter}
                    />
                </div>
            </div>
        );
    }
}

export default CircleOfWillis;