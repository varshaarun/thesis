import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Fab, FormControl, IconButton, InputLabel, MenuItem, Popover, Select, Typography} from "@material-ui/core";
import SettingsIcon from '@material-ui/icons/Settings';

class OperationSelector extends Component {

    constructor(props) {
        super(props);
        this.state = {
            anchorEl: null
        }
        this.ALLOWED_OPERATION = [
            {label: "And", value: "AND"},
            {label: "Or", value: "OR"}
        ]
    }

    changeState = (stateName, stateValue) => {
        this.setState({[stateName]: stateValue})
    }

    handleChange = (label, value) => {
        const {glyphOperator, heatmapOperator} = this.props;
        this.props.onChange({
            ...{ glyphOperator, heatmapOperator },
            [label]: value
        });
    }


    getSelect = (label, operatorObjectName, selectedValue) => (
        <FormControl style={{margin: "10px 0"}}>
            <InputLabel>{label}</InputLabel>
            <Select value={selectedValue}
                    style={{width: 200}}
                    onChange={(e) => this.handleChange(operatorObjectName, e.target.value)}>
                {this.ALLOWED_OPERATION.map((operation) => (
                    <MenuItem key={`${label}_${operation.label}`} value={operation.value}>
                        {operation.label}
                    </MenuItem>
                ))}
            </Select>
        </FormControl>
    )

    render() {
        const {anchorEl} = this.state;
        const {glyphOperator, heatmapOperator} = this.props;
        return (
            <>
                <IconButton onClick={(e) => this.changeState("anchorEl", e.target)}>
                    <SettingsIcon color={"primary"}/>
                </IconButton>
                <Popover open={Boolean(anchorEl)} onClose={() => this.changeState("anchorEl", null)}
                         anchorEl={this.state.anchorEl}
                         anchorOrigin={{
                             vertical: 'bottom',
                             horizontal: 'center',
                         }}
                         transformOrigin={{
                             vertical: 'top',
                             horizontal: 'center',
                         }}
                >
                    <div style={{maxWidth: 210, padding: 10}}>
                        <Typography color={"textSecondary"}> Operation within filters </Typography>
                        {this.getSelect("Glyph", "glyphOperator", glyphOperator)}
                        {this.getSelect("Heatmap", "heatmapOperator", heatmapOperator)}
                    </div>
                </Popover>
            </>
        );
    }
}

OperationSelector.propTypes = {
    onChange: PropTypes.func.isRequired,
    glyphOperator: PropTypes.string.isRequired,
    heatmapOperator: PropTypes.string.isRequired
};

export default OperationSelector;