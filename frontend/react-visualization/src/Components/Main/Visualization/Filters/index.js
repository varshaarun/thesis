import React, {Component} from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import {
    Avatar,
    Button, Checkbox,
    Chip, FormControl,
    FormControlLabel,
    FormGroup,
    FormLabel, Radio, RadioGroup,
    Slider,
    TextField,
    Tooltip,
    Typography, withStyles
} from "@material-ui/core";
import CheckIcon from '@material-ui/icons/Check';
import "./index.css"
import OperationSelector from "./OperationSelector";

class Filters extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currentLeftFilter: {key: null, value: [], partOf: 'glyph'},
            currentRightFilter: {key: null, value: [], partOf: 'glyph'},
        }
        this.defaultAgeLimit = [25, 75];
    }

    //copyToStateName = leftFilter or rightFilter; copyFromStateName = currentLeftFilter or currentRightFilter
    //To copy from currentFilter to either left or right filter to send to the backend
    addToFilter = (copyFromStateName, copyToStateName) => {
        const filterCopy = [...this.props[copyToStateName]];
        filterCopy.push(this.state[copyFromStateName]);
        this.setState({
            // [copyToStateName]: filterCopy,
            [copyFromStateName]: {key: null, value: [], partOf: 'glyph'}
        });
        this.props.modifyFilter(copyToStateName, filterCopy);
    }

    //this is opposite to add filter function
    //copyFromStateName = leftFilter or rightFilter; copyToStateName = currentLeftFilter or currentRightFilter
    editFilter = async (copyFromStateName, index, copyToStateName) => {
        const existingFilter = {...this.state[copyToStateName]};
        const filterCopy = [...this.props[copyFromStateName]];
        const currentFilterCopy = filterCopy.splice(index, 1)[0];
        if(existingFilter.key !== null && existingFilter.value.length !== 0 ) {
            filterCopy.push(existingFilter);
        }
        this.setState({
            [copyToStateName]: currentFilterCopy,
            // [copyFromStateName]: filterCopy
        });
        this.props.modifyFilter(copyFromStateName, filterCopy);
    }

    deleteFilter = (stateName, index) => {
        const filterCopy = [...this.props[stateName]];
        filterCopy.splice(index, 1);
        // this.setState({[stateName]: filterCopy});
        this.props.modifyFilter(stateName, filterCopy);
    }

    generateChangeStateValue = (stateName, keyName, value) => {
        const stateValue = {
            ...this.state[stateName],
            [keyName]: value
        }
        if(keyName === 'key' && value === null) {
            stateValue['value'] = [];
        }
        this.setState({[stateName]: {...stateValue}})
    }

    disableCheck = (option, filterKey, stateName) => {
        let found = false;
        if(filterKey) {//filter check within state
            stateName = stateName === "currentLeftFilter" ? "leftFilter" : "rightFilter";
            this.props[stateName].forEach(filters => {
                if(filters.key === option) {
                    found = true;
                    return 0;
                }
            })
        }
        return found;
    }

    getFilterComponent = (color, multiSelect, value, options, filterKey, filterLabel, filterWidth, stateName, keyName) => {
        return (
            <Autocomplete
                id={filterLabel}
                multiple={multiSelect}
                value={value}
                options={options}
                disableCloseOnSelect={!filterKey}
                getOptionDisabled={option => this.disableCheck(option, filterKey, stateName)}
                getOptionLabel={option => filterKey ? `${option.split(/(?=[A-Z])/).join(" ")[0].toUpperCase()}${option.split(/(?=[A-Z])/).join(" ").slice(1)}` : option}
                renderInput={(params) => <TextField {...params} label={filterLabel} variant="outlined" color={color}/>}
                style={{width: filterWidth , marginTop: "10px"}}
                onChange={(e,value) => this.generateChangeStateValue(stateName, keyName, value)}
            />
        )
    }

    getAgeSlider = (stateName, keyName, value, color) => {
        return (
            <div id={"age-slider"}>
                <Typography>Age Range</Typography>
                <Slider
                    color={color}
                    value={value}
                    min={1}
                    max={99}
                    step={1}
                    valueLabelDisplay={"on"}
                    onChange={(event, value) => this.generateChangeStateValue(stateName, keyName, value)}
                />
            </div>
        );
    }

    getAppliedFilterChip = (filterStateName, currentFilterStateName, color) => {
        return (
            this.props[filterStateName].map((data, index) =>
                <Tooltip title={data.key !== "age" ? `Applied Filters: ${data.value.join(", ")}` : `Age Limit: ${data.value[0]} - ${data.value[1]}`}
                         key={data.key}>
                    <Chip color={color} style={{margin: 2}} variant={"outlined"}
                          avatar={<Avatar>{data.partOf[0].toUpperCase()}</Avatar>}
                          label={`${data.key.split(/(?=[A-Z])/).join(" ")[0].toUpperCase()}${data.key.split(/(?=[A-Z])/).join(" ").slice(1)}`}
                          onDelete={() => this.deleteFilter(filterStateName, index)}
                          onClick={() => this.editFilter(filterStateName, index, currentFilterStateName)}
                    />
                </Tooltip>
            )
        );
    }

    render() {
        const {leftOperators, rightOperators} = this.props;
        return (
            <div id={"filter-container"}>
                <div id={"left-filter"}>
                    <div id={"filter-key"}>
                        {/* filter key drop down */}
                        {this.getFilterComponent("primary",false, this.state.currentLeftFilter.key ? this.state.currentLeftFilter.key : null,
                            ["age", ...Object.keys(this.props.filterData).filter(value => value !== "aneurysmLocation")], true, "Left Filter", 300, "currentLeftFilter", "key")}
                        <OperationSelector onChange={(value) => this.props.modifyFilter("leftOperators", value)}
                                           glyphOperator={leftOperators.glyphOperator} heatmapOperator={leftOperators.heatmapOperator}/>
                    </div>
                    <div id={"filter-value"}>
                        {/* filter value drop down */}
                        {this.state.currentLeftFilter.key  && this.state.currentLeftFilter.key !== "age" &&
                        this.getFilterComponent("primary",true,
                            this.state.currentLeftFilter.value.length !== 0 ? this.state.currentLeftFilter.value : [],
                            this.props.filterData[this.state.currentLeftFilter.key], false, "Filter Options", 300, "currentLeftFilter", "value")}
                        {/*age slider*/}
                        {this.state.currentLeftFilter.key  && this.state.currentLeftFilter.key === "age" &&
                        this.getAgeSlider("currentLeftFilter", "value", this.state.currentLeftFilter.value.length !== 0 ? this.state.currentLeftFilter.value : this.defaultAgeLimit, "primary")}
                        {/*Glyph or heatmap selection with apply button*/}
                        {this.state.currentLeftFilter.value.length !== 0 &&
                        <div id={"filter-button-container"}>
                            <FormControl>
                                <FormLabel>Filter part of</FormLabel>
                                <RadioGroup row name="partOf" value={this.state.currentLeftFilter.partOf}
                                            onChange={(event, value) => this.generateChangeStateValue("currentLeftFilter", "partOf", value)}>
                                    <FormControlLabel control={<Radio color={"primary"}/>} label={"Glyph"} value={"glyph"}/>
                                    <FormControlLabel control={<Radio color={"primary"}/>} label={"Heatmap"} value={"heatmap"}/>
                                </RadioGroup>
                            </FormControl>
                            <Button size={"small"} id={"apply-filter-button"}
                                    variant={"contained"} color={"primary"}
                                    onClick={() => this.addToFilter("currentLeftFilter", "leftFilter")}>
                                Apply
                            </Button>
                        </div>
                        }
                    </div>
                    <div id={"selected-filters"}>
                        {this.getAppliedFilterChip("leftFilter", "currentLeftFilter", "primary")}
                    </div>
                </div>
                <div id={"right-filter"}>
                    <div id={"filter-key"}>
                        {/* filter key drop down */}
                        {this.getFilterComponent("primary",false,this.state.currentRightFilter.key ? this.state.currentRightFilter.key : null,
                            ["age", ...Object.keys(this.props.filterData).filter(value => value !== "aneurysmLocation")], true, "Right Filter", 300, "currentRightFilter", "key")}
                        <OperationSelector onChange={(value) => this.props.modifyFilter("rightOperators", value)}
                                           glyphOperator={rightOperators.glyphOperator} heatmapOperator={rightOperators.heatmapOperator}/>
                    </div>
                    <div id={"filter-value"}>
                        {/* filter value drop down */}
                        {this.state.currentRightFilter.key && this.state.currentRightFilter.key !== "age" &&
                        this.getFilterComponent("primary",true,this.state.currentRightFilter.value.length !== 0 ? this.state.currentRightFilter.value : [],
                            this.props.filterData[this.state.currentRightFilter.key], false, "Filter Options", 300, "currentRightFilter", "value")}
                        {/*age slider*/}
                        {this.state.currentRightFilter.key  && this.state.currentRightFilter.key === "age" &&
                        this.getAgeSlider("currentRightFilter", "value", this.state.currentRightFilter.value.length !== 0 ? this.state.currentRightFilter.value : this.defaultAgeLimit, "primary")}
                        {/*Glyph or heatmap selection with apply button*/}
                        {this.state.currentRightFilter.value.length !== 0 &&
                        <div id={"filter-button-container"}>
                            <FormControl>
                                <FormLabel>Filter part of</FormLabel>
                                <RadioGroup row name="partOf" value={this.state.currentRightFilter.partOf}
                                            onChange={(event, value) => this.generateChangeStateValue("currentRightFilter", "partOf", value)}>
                                    <FormControlLabel control={<Radio color={"primary"}/>} label={"Glyph"} value={"glyph"}/>
                                    <FormControlLabel control={<Radio color={"primary"}/>} label={"Heatmap"} value={"heatmap"}/>
                                </RadioGroup>
                            </FormControl>
                            <Button id={"apply-filter-button"} size={"small"} variant={"contained"} color={"primary"} onClick={() => this.addToFilter("currentRightFilter", "rightFilter")}>
                                Apply
                            </Button>
                        </div>}
                    </div>
                    <div id={"selected-filters"}>
                        {this.getAppliedFilterChip("rightFilter", "currentRightFilter", "primary")}
                    </div>
                </div>
            </div>
        );
    }
}

export default Filters;