import {LEFT_SIDE_FILTER, NAMES} from "../../../../Helper/Constants/filterSidesName";
import {BAR_PLOT, LINE_PLOT, RADAR_PLOT, STACKED_BAR_PLOT} from "../../../../Helper/Constants/plots";

const getColor = (color, alpha=1) => {
    const {r,g,b} = color;
    return `rgba(${r},${g},${b},${alpha})`;
}

const configProvider = (chartType, filterData, aneurysmLocations, leftFilterPart, rightFilterPart, leftSideColor, rightSideColor) => {
    const chartConfig = {};
    chartConfig.data = {};
    //chart type
    chartConfig.type = chartType === STACKED_BAR_PLOT ? BAR_PLOT : chartType;
    //chart x-axis
    chartConfig.data.labels = aneurysmLocations;
    const datasets = [];
    NAMES.map(filterSide => {
        let dataset = {
            label: filterSide === LEFT_SIDE_FILTER ? "Left Filter" : "Right Filter",
            data: [],
        }
        if(chartType === RADAR_PLOT || chartType === LINE_PLOT ) {
            dataset.backgroundColor = filterSide === LEFT_SIDE_FILTER ? getColor(leftSideColor, 0.2) : getColor(rightSideColor, 0.2);
            dataset.borderColor = filterSide === LEFT_SIDE_FILTER ? getColor(leftSideColor) : getColor(rightSideColor);
            dataset.pointBackgroundColor = filterSide === LEFT_SIDE_FILTER ? getColor(leftSideColor) : getColor(rightSideColor);
        }
        else {
            dataset.backgroundColor = filterSide === LEFT_SIDE_FILTER ? getColor(leftSideColor) : getColor(rightSideColor);
        }
        let partOf = filterSide === LEFT_SIDE_FILTER ? leftFilterPart : rightFilterPart;
        aneurysmLocations.map(location => {
            const locationData = filterData[filterSide][partOf][location]
            dataset.data.push(locationData.length)
        })
        datasets.push(dataset);
    })
    chartConfig.data.datasets = datasets;
    if(chartType === STACKED_BAR_PLOT) {
        chartConfig.options = {
            scales: {
                x: {
                    stacked: true,
                },
                y: {
                    stacked: true
                }
            }
        }
    }
    return chartConfig;
}

export default configProvider;