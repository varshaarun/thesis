import React, {createRef, PureComponent} from 'react';
import {Chart, registerables} from "chart.js";
import PropTypes from "prop-types";
import configProvider from "./configProvider";
import {GLYPH, PART_OF} from "../../../../Helper/Constants/partOf"
import {FormControl, InputLabel, MenuItem, Select} from "@material-ui/core";
import {BAR_PLOT, PLOT_TYPES} from "../../../../Helper/Constants/plots";
//import "chartjs-chart-box-and-violin-plot";
import { BoxPlotController } from '@sgratzl/chartjs-chart-boxplot';


class PlotsContainer extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            leftSidePartOf: GLYPH,
            rightSidePartOf: GLYPH,
            plotType: BAR_PLOT,
            //plotType: "boxplot",
            leftSideColor: {r: 250, g: 89, b: 81, a: 1},
            rightSideColor: {r: 0, g: 0, b: 0, a: 1}
        };
        Chart.register(...registerables);
        console.log(BoxPlotController, registerables)
        this.canvasRef = createRef();
        this.chart = null;
    }

    changeState = (stateName, stateValue) => {
        this.setState({ [stateName]: stateValue })
    };

    componentDidMount() {
        this.renderPlot();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        this.renderPlot();
    }

    renderPlot = () => {
        const { filterResults, aneurysmLocation } = this.props;
        const { leftSidePartOf, rightSidePartOf, plotType, leftSideColor, rightSideColor } = this.state;
        if(this.chart) this.chart.destroy();

        const chartRef = this.canvasRef.current.getContext("2d")
        this.chart = new Chart(chartRef,
            configProvider(plotType, filterResults, aneurysmLocation, leftSidePartOf, rightSidePartOf, leftSideColor, rightSideColor)
        );
    }

    getSelect = (label, selectedValue, options, stateName, selectSize) => (
        <FormControl style={{margin: "10px 0"}}>
            <InputLabel>{label}</InputLabel>
            <Select value={selectedValue}
                    style={{width: selectSize}}
                    onChange={(e) => this.changeState(stateName, e.target.value)}>
                {options.map((operation) => (
                    <MenuItem key={`${label}_${operation.label}`} value={operation.value}>
                        {operation.label}
                    </MenuItem>
                ))}
            </Select>
        </FormControl>
    )

    render() {
        return (
            <>
                <div style={{display: "flex", justifyContent: "center"}}>
                    {this.getSelect("Left Side part", this.state.leftSidePartOf, PART_OF, "leftSidePartOf", 150)}
                    {this.getSelect("Plot type", this.state.plotType, PLOT_TYPES, "plotType", 200)}
                    {this.getSelect("Right Side part", this.state.rightSidePartOf, PART_OF, "rightSidePartOf", 150)}
                </div>
                <div style={{display: "flex", justifyContent: "center", marginBottom: 50}}>
                    <div style={{width: 800, height: 600}}>
                        <canvas width={800} height={600} ref={this.canvasRef}/>
                    </div>
                </div>
            </>
        );
    }
}

PlotsContainer.propTypes = {
    filterResults: PropTypes.object.isRequired,
    aneurysmLocation: PropTypes.array.isRequired,
}

export default PlotsContainer;