import React, {Component} from 'react';
import "./index.css"
import Filters from "./Filters";
import AneurysmLocation from "./Aneurysm Location";
import {getFilterResult} from "../../../Service/httpClient";
import {Tab, Tabs} from "@material-ui/core";
import CircleOfWillis from "./Circle Of Willis";
import PlotsContainer from "./Plots"

const removeUnwantedLocations = (aneurysmLocation) => {
    aneurysmLocation.splice(aneurysmLocation.indexOf("NA"), 1);
    aneurysmLocation.splice(aneurysmLocation.indexOf("other"), 1);
    return aneurysmLocation;
}

class Visualization extends Component {

    constructor(props) {
        super(props);
        this.state = {
            leftFilter: [
                {
                    "key": "sex",
                    "value": ["F"],
                    "partOf": "glyph"
                },
                {
                    "key": "institution",
                    "value": [
                        "UniversityHospitalMagdeburg"
                    ],
                    "partOf": "heatmap"
                }
            ],
            rightFilter: [
                {
                    "key": "sex",
                    "value": [
                        "M"
                    ],
                    "partOf": "glyph"
                },
                {
                    "key": "institution",
                    "value": ["UniversityHospitalMagdeburg"
                    ],
                    "partOf": "heatmap"
                }
            ],
            leftOperators: {
                glyphOperator: "AND",
                heatmapOperator: "AND"
            },
            rightOperators: {
                glyphOperator: "AND",
                heatmapOperator: "AND"
            },
            aneurysmLocation: removeUnwantedLocations(this.props.filterData.aneurysmLocation),
            appliedFilters: null,
            filterResults: null,
            currentTab: 0,
        }
        this.tabsName = ["Circle Of Willis", "Plots"]
    }


    //  componentDidCatch(error, errorInfo) {
    //     alert('Error')
    //     console.log(error, errorInfo)
    //
    // }

    changeState = (stateName, stateValue) => {
        this.setState({[stateName]: stateValue});
    }

    getRequestBody = () => {
        const requestBody = {leftFilter: { glyph: [], heatmap: [] }, rightFilter: { glyph: [], heatmap: [] }};
        ["leftFilter", "rightFilter"].map(filterSide => {
            this.state[filterSide].map(filterData => {
                const { key, value, partOf } = filterData;
                requestBody[filterSide][partOf].push({key, value});
            });
            const operators = filterSide === "leftFilter" ? this.state.leftOperators : this.state.rightOperators;
            requestBody[filterSide] = {
                ...requestBody[filterSide],
                ...operators
            }
         })
        requestBody["aneurysmLocation"] = this.state.aneurysmLocation;
        return requestBody;
    }

    filterQuery = async () => {
        const requestBody = this.getRequestBody();
        let response = await getFilterResult(requestBody);
        this.changeState("appliedFilters", requestBody);
        this.changeState("filterResults", response);
    }

    render() {
        const {leftFilter, rightFilter, filterResults, leftOperators,
                rightOperators, aneurysmLocation, currentTab, appliedFilters } = this.state;
        return (
            <div id={"visualization-panel"}>
                {/*tabs like circleofwillis, plots*/}
                <Tabs centered indicatorColor={"primary"} textColor={"primary"} value={currentTab}
                      onChange={(e,tabIndex) => this.changeState("currentTab", tabIndex)}>
                    {this.tabsName.map(tabName => <Tab key={tabName} label={tabName}/>)}
                </Tabs>
                <Filters leftFilter={leftFilter} rightFilter={rightFilter}
                         leftOperators={leftOperators} rightOperators={rightOperators}
                         filterData={this.props.filterData} modifyFilter={this.changeState}/>
                <AneurysmLocation aneurysmLocation={this.props.filterData.aneurysmLocation}
                                  modifyFilter={this.changeState} filterQuery={this.filterQuery}/>
                {currentTab === 0 && filterResults && <CircleOfWillis appliedFilter={appliedFilters} filterResults={filterResults}/>}
                {currentTab === 1 && filterResults && <PlotsContainer aneurysmLocation={aneurysmLocation} filterResults={filterResults} />}
            </div>
        );
    }
}

export default Visualization;