export const GLYPH_DETAILS_COLOR_PALETTES = [
    ['#c9ffbf', '#ffafbd'],
    ['#d53369', '#cbad6d'],
    ['#5f2c82', '#49a09d'],
    ['#649173', '#DBD5A4'],
    // ['#f2709c', '#ff9472' ],
    // ['#ddd6f3', '#faaca8'],
    // ['#FC354C', '#0ABFBC'],
    ['#DC2424', '#4A569D'],
    // ['#1D2B64', '#F8CDDA'],
    ['#E55D87', '#5FC3E4'],
    ['#003973', '#E5E5BE'],
    ['#3D7EAA', '#FFE47A'],
]