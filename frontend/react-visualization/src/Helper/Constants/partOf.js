export const GLYPH = "glyph";

export const HEATMAP = "heatmap";

export const PART_OF = [{label: "Glyph", value: GLYPH}, {label: "Heatmap", value: HEATMAP}];
