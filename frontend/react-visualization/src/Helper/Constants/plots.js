export const BAR_PLOT = "bar";

export const RADAR_PLOT = "radar";

export const LINE_PLOT = "line";

export const STACKED_BAR_PLOT = "stackedBarPlot";

export const BOX_PLOT = "boxplot"

export const PLOT_TYPES = [
    {label: "Bar plot", value: BAR_PLOT},
    {label: "Radar plot", value: RADAR_PLOT},
    {label: "Stacked bar chart", value: STACKED_BAR_PLOT},
    {label: "Line chart", value: LINE_PLOT},
];