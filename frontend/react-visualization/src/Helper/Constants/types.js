export const GLYPH_HEATMAP_TYPE = [
    {label: "Local", value: "localMaxima"},
    {label: "Global", value: "globalMaxima"},
    {label: "Overall", value: "overall"}
];