// TODO AnteriorSpinalArtery to be added

export const PARTS_MAPPING = {
	"M1": {
			"leftFilter": { cx:49.926, cy:81.822 },
			"rightFilter": { cx:164.66, cy:84.596 }
		},
	"basilarTip": {
			"leftFilter": { cx:100.84, cy:138.82 },
			"rightFilter": { cx:110.94, cy:138.82 }
		},
	"CarotidT": {
			"leftFilter": { cx:73.152, cy:84.441 },
			"rightFilter": { cx:138.82, cy:85.751 }
		},
	"A1": {
			"leftFilter": { cx:85.876, cy:79.63 },
			"rightFilter": { cx:128.81, cy:81.192 }
		},
	"pericallA": {
			"leftFilter": { cx:81.074, cy:57.404 },
			"rightFilter": { cx:130.96, cy:58.901 }
		},
	"PCA": {
			"leftFilter": { cx:63.487, cy:153.79 },
			"rightFilter": { cx:153.16, cy:155.35 }
		},
	 "ophthal": {
	 		"leftFilter": { cx:94.591, cy:91.839 },
	 		"rightFilter": { cx:119.92, cy:92.609 }
	 	},
	"AchoA": {
			"leftFilter": { cx:65.732, cy:105.14 },
			"rightFilter": { cx:147.92, cy:104.21 }
		},
	"M3": {
			"leftFilter": {cx:36.824, cy:143.25 },
			"rightFilter": { cx:174.93, cy:140.5 }
		},
	"Pcom": {
			"leftFilter": { cx:78.267, cy:131.34 },
			"rightFilter": { cx:136.51, cy:132.08 }
		},
	"MCA-Bif": {
			"leftFilter": { cx:25.695, cy:92.924 },
			"rightFilter": { cx:183.06, cy:94.138 }
		},
	"M2": {
			"leftFilter": { cx:6.7994, cy:107.95 },
			"rightFilter": { cx:201.06, cy:108.32 }
		},
	"PICA": {
			"leftFilter": { cx:62.178, cy:267.54 },
			"rightFilter": { cx:146.92, cy:268.29 }
		},
	"Acom": {
			"leftFilter": { cx:103.27, cy:73.9 },
			"rightFilter": { cx:112.25, cy:73.9 }
		},
	"AICA":  {
			"leftFilter": { cx:65.171, cy:203.18 },
			"rightFilter": { cx:148.86, cy:204.18 }
		}

}