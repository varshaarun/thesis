import axios from "axios";
import {fileUploadURL, getFilterResponseURL, getFilterURL, getIndividualFilterDataURL} from "./requestURLCollection";

export const fileUploadRequest = async (body) => {
    try{
        let response = await axios.post(fileUploadURL,body,{
            header: {"Content-type": "multipart/form-data"},
        });
        return response.data;
    }
    catch (error) {
        throw error.response;
    }
}

export const getFilters = async () => {
    try {
        let response = await axios.get(getFilterURL);
        return response.data;
    }
    catch (error) {
        throw error.response;
    }
}

export const getFilterResult = async (body) => {
    try {
        let response = await axios.post(getFilterResponseURL, body);
        return response.data;
    }
    catch (error) {
        throw error.response;
    }
}

export const getIndividualFilterResult = async (body) => {
    try {
        let response = await axios.post(getIndividualFilterDataURL, body);
        return response.data;
    }
    catch (error) {
        throw error.response;
    }
}