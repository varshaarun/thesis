import {hostPort, hostURL} from "./hostConfiguration";

export const fileUploadURL = `${hostURL}:${hostPort}/patient/file/`;

export const getFilterURL = `${hostURL}:${hostPort}/patient/distinct/`;

export const getFilterResponseURL = `${hostURL}:${hostPort}/patient/query/`;

export const getIndividualFilterDataURL = `${hostURL}:${hostPort}/patient/query/extension/`;